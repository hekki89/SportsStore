﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Models
{
    public interface IProductRepository
    {
        // IQueryable<T> is derived from IEnumerable<T>. More effective in case of SQL repos.
        IQueryable<Product> Products { get; }
    }
}
